package by.shag.golatina.service;

import by.shag.golatina.api.dto.PostDto;
import by.shag.golatina.jpa.model.Post;
import by.shag.golatina.jpa.repository.PostRepository;
import by.shag.golatina.mapping.PostDtoMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class PostServiceTest {

    private static Post post;
    private static Post postAfterOperation;
    private static PostDto postDto;
    private static PostDto postDtoAfterOperation;
    private static List<Post> postList;
    private static List<PostDto> postDtoList;
    private static Long number;

    @InjectMocks
    private PostService service;

    @Mock
    private PostRepository repository;
    @Mock
    private PostDtoMapper mapper;

    @BeforeEach
    void start() {
        post = new Post(1L, Instant.now(), "Test-Login11", "Test-Content11", 110);
        postAfterOperation = new Post(2L, Instant.now().plus(5, ChronoUnit.MINUTES), "Test-Login12", "Test-Content12", 120);
        postDto = new PostDto(10L, Instant.now().minus(5, ChronoUnit.MINUTES), "Test-Login10", "Test-Content10", 10);
        postDtoAfterOperation = new PostDto(20L, Instant.now().minus(5, ChronoUnit.MINUTES), "Test-Login20", "Test-Content20", 20);
        postList = Arrays.asList(post, post, post);
        postDtoList = Arrays.asList(postDtoAfterOperation, postDtoAfterOperation, postDtoAfterOperation);
        number = post.getId();
    }

    @AfterEach
    void finish() {
        verifyNoMoreInteractions(mapper, repository);
    }

    @Test
    void save() {
        when(mapper.mapFromPostDto(any(PostDto.class))).thenReturn(post);
        when(repository.save(any(Post.class))).thenReturn(postAfterOperation);
        when(mapper.mapFromPost(any(Post.class))).thenReturn(postDtoAfterOperation);
        PostDto result = service.save(postDto);
        assertEquals(result, postDtoAfterOperation);
        verify(mapper).mapFromPostDto(postDto);
        verify(repository).save(post);
        verify(mapper).mapFromPost(postAfterOperation);
    }

    @Test
    void findByID() {
        when(repository.findByID(anyLong())).thenReturn(post);
        when(mapper.mapFromPost(any(Post.class))).thenReturn(postDtoAfterOperation);
        PostDto result = service.findByID(number);
        assertEquals(result, postDtoAfterOperation);
        verify(repository).findByID(number);
        verify(mapper).mapFromPost(post);
    }

    @Test
    void findAll() {
        when(repository.findAll()).thenReturn(postList);
        when(mapper.mapFromPost(any(Post.class))).thenReturn(postDtoAfterOperation);
        List<PostDto> result = service.findAll();
        assertArrayEquals(result.toArray(), postDtoList.toArray());
        verify(mapper, times(3)).mapFromPost(post);
        verify(repository).findAll();
    }

    @Test
    void update() {
        when(mapper.mapFromPostDto(any(PostDto.class))).thenReturn(post);
        when(repository.update(any(Post.class))).thenReturn(postAfterOperation);
        when(mapper.mapFromPost(any(Post.class))).thenReturn(postDtoAfterOperation);
        PostDto result = service.update(postDto);
        assertEquals(result, postDtoAfterOperation);
        verify(mapper).mapFromPostDto(postDto);
        verify(repository).update(post);
        verify(mapper).mapFromPost(postAfterOperation);
    }

    @Test
    void deleteByID() {
        service.deleteByID(number);
        verify(repository).deleteByID(number);
    }
}