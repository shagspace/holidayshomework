package by.shag.golatina.mapping;

import by.shag.golatina.api.dto.PostDto;
import by.shag.golatina.jpa.model.Post;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PostDtoMapperTest {

    private final Long ID = 1L;
    private final Instant UPDATE_TIME = Clock.tickSeconds(ZoneId.of("UTC+3")).instant();
    private final String LOGIN = "Login";
    private final String CONTENT = "Мне кажется, что именно с этим обстоятельством связана местная легенда, будто у старых слонов из квантовых джунглей длинная " +
            "шерсть. Я полагаю, что на не столь крупных животных, обитающих в квантовых джунглях, замечательные квантовые эффекты будут более заметными.";
    private final Integer APPROVAL_REACTION = 100;
    private PostDtoMapper dtoMapper = new PostDtoMapper();

    @Test
    void mapFromPost() {
        Post post = new Post(ID, UPDATE_TIME, LOGIN, CONTENT, APPROVAL_REACTION);

        PostDto result = dtoMapper.mapFromPost(post);
        assertEquals(result.getId(), ID);
        assertEquals(result.getUpdateDate(), UPDATE_TIME);
        assertEquals(result.getUserLogin(), LOGIN);
        assertEquals(result.getContentData(), CONTENT);
        assertEquals(result.getApprovalReaction(), APPROVAL_REACTION);
    }

    @Test
    void mapFromPostDto() {
        PostDto postDto = new PostDto(ID, UPDATE_TIME, LOGIN, CONTENT, APPROVAL_REACTION);

        Post result = dtoMapper.mapFromPostDto(postDto);
        assertEquals(result.getId(), ID);
        assertEquals(result.getUpdateDate(), UPDATE_TIME);
        assertEquals(result.getUserLogin(), LOGIN);
        assertEquals(result.getContentData(), CONTENT);
        assertEquals(result.getApprovalReaction(), APPROVAL_REACTION);
    }
}