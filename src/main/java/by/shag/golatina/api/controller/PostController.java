package by.shag.golatina.api.controller;

import by.shag.golatina.api.dto.PostDto;
import by.shag.golatina.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class PostController {

    @Autowired
    private PostService service;

    @GetMapping(value = "/start")
    public String startPage() {
        return "start.html";
    }

    @GetMapping(value = "/posts")
    public String findAll(Model model) {
        List<PostDto> all = service.findAll();
        model.addAttribute("posts", all);
        return "posts.html";
    }

    @GetMapping(value = "/post/{id}")
    public String findById(@RequestParam(name = "id") Integer id, Model model) {
        model.addAttribute("post", service.findByID(Long.valueOf(id)));
        return "post.html";
    }

    @GetMapping(value = "/post/create-form")
    public String getCreateForm(Model model) {
        model.addAttribute("postDto", new PostDto());
        return "createForm.html";
    }

    @PostMapping(value = "/post/create")
    public String createForm(@ModelAttribute("postDto") PostDto postDto) {
        postDto.setApprovalReaction(0);
        service.save(postDto);
        return startPage();
    }

    @GetMapping(value = "/posts/search")
    public String findContent(@RequestParam(name = "content") String content, Model model) {
        List<PostDto> all = service.findAll().stream()
                .filter(s -> s.getContentData().contains(content))
                .collect(Collectors.toList());
        model.addAttribute("posts", all);
        return "posts.html";
    }

    @PostMapping("/posts/like")
    public String likeComment(@ModelAttribute(name = "id") Integer id) {
        PostDto update = service.findByID(Long.valueOf(id));
        Integer like = update.getApprovalReaction() + 1;
        update.setApprovalReaction(like);
        service.update(update);
        return startPage();
    }

}
