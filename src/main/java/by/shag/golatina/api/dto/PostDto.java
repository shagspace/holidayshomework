package by.shag.golatina.api.dto;

import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class PostDto {

    private Long id;
    private String userLogin;
    private Instant updateDate;
    private String contentData;
    private Integer approvalReaction;

    public PostDto() {
    }

    public PostDto(Long id, Instant updateDate, String userLogin, String contentData, Integer approvalReaction) {
        this.id = id;
        this.userLogin = userLogin;
        this.updateDate = updateDate;
        this.contentData = contentData;
        this.approvalReaction = approvalReaction;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public String getContentData() {
        return contentData;
    }

    public void setContentData(String contentData) {
        this.contentData = contentData;
    }

    public Integer getApprovalReaction() {
        return approvalReaction;
    }

    public void setApprovalReaction(Integer approvalReaction) {
        this.approvalReaction = approvalReaction;
    }

}
