package by.shag.golatina.mapping;

import by.shag.golatina.api.dto.PostDto;
import by.shag.golatina.jpa.model.Post;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.ZoneId;

@Component
public class PostDtoMapper {

    public Post mapFromPostDto(PostDto postDto) {
        Post post = new Post();
        post.setId(postDto.getId());
        post.setUpdateDate(Clock.tickSeconds(ZoneId.of("UTC+3")).instant());
        post.setUserLogin(postDto.getUserLogin());
        post.setContentData(postDto.getContentData());
        post.setApprovalReaction(postDto.getApprovalReaction());
        return post;
    }

    public PostDto mapFromPost(Post post) {
        PostDto postDto = new PostDto();
        postDto.setId(post.getId());
        postDto.setUpdateDate(post.getUpdateDate());
        postDto.setUserLogin(post.getUserLogin());
        postDto.setContentData(post.getContentData());
        postDto.setApprovalReaction(post.getApprovalReaction());
        return postDto;
    }

}
