package by.shag.golatina.service;

import by.shag.golatina.api.dto.PostDto;
import by.shag.golatina.jpa.repository.PostRepository;
import by.shag.golatina.mapping.PostDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostService implements ServiceCRUD<PostDto, Long> {

    @Autowired
    private PostRepository repository;
    @Autowired
    private PostDtoMapper mapper;

    @Override
    public PostDto save(PostDto entity) {
        return mapper.mapFromPost(repository.save(mapper.mapFromPostDto(entity)));
    }

    @Override
    public PostDto findByID(Long aLong) {
        return mapper.mapFromPost(repository.findByID(aLong));
    }

    @Override
    public List<PostDto> findAll() {
        return repository.findAll().stream()
                .map(mapper::mapFromPost)
                .sorted(Comparator.comparing(PostDto::getId))
                .collect(Collectors.toList());
    }

    @Override
    public PostDto update(PostDto entity) {
        return mapper.mapFromPost(repository.update(mapper.mapFromPostDto(entity)));
    }

    @Override
    public void deleteByID(Long aLong) {
        repository.deleteByID(aLong);
    }
}
