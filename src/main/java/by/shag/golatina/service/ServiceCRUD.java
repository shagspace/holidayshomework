package by.shag.golatina.service;

import java.util.List;

public interface ServiceCRUD<E, ID> {

    E save(E entity);

    E findByID(ID id);

    List<E> findAll();

    E update(E entity);

    void deleteByID(ID id);
}
