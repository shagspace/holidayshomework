package by.shag.golatina.jpa.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.Instant;

@Component
@Entity
@Table(name = "Post")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_login", nullable = false)
    private String userLogin;

    @Column(name = "update_at", nullable = false)
    private Instant updateDate;

    @Column(name = "content_data", nullable = false)
    private String contentData;

    @Column(name = "approval_reaction", nullable = false)
    private Integer approvalReaction;

    public Post(Long id, Instant updateDate, String userLogin, String contentData, Integer approvalReaction) {
        this.id = id;
        this.userLogin = userLogin;
        this.updateDate = updateDate;
        this.contentData = contentData;
        this.approvalReaction = approvalReaction;
    }

    public Post() {
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", userLogin='" + userLogin + '\'' +
                ", updateDate=" + updateDate +
                ", contentData='" + contentData + '\'' +
                ", approvalReaction=" + approvalReaction +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant createdDate) {
        this.updateDate = createdDate;
    }

    public String getContentData() {
        return contentData;
    }

    public void setContentData(String contentData) {
        this.contentData = contentData;
    }

    public Integer getApprovalReaction() {
        return approvalReaction;
    }

    public void setApprovalReaction(Integer approvalReaction) {
        this.approvalReaction = approvalReaction;
    }
}
