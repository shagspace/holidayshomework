package by.shag.golatina.jpa.repository;

import by.shag.golatina.jpa.model.Post;
import by.shag.golatina.util.EntityManagerUtil;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

@Repository
public class PostRepository implements CRUDRepository<Post, Long> {

    @Override
    public Post save(Post entity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            em.persist(entity);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
        em.close();
        return entity;
    }

    @Override
    public Post findByID(Long aLong) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        Post post = em.find(Post.class, aLong);
        em.close();
        return post;
    }

    @Override
    public List<Post> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        List<Post> result = em.createQuery("SELECT n FROM Post n").getResultList();
        em.close();
        return result;
    }

    @Override
    public Post update(Post entity) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            Post updatePost = em.find(Post.class, entity.getId());
            updatePost.setUserLogin(entity.getUserLogin());
            updatePost.setUpdateDate(entity.getUpdateDate());
            updatePost.setContentData(entity.getContentData());
            updatePost.setApprovalReaction(entity.getApprovalReaction());
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
        em.close();
        return entity;
    }

    @Override
    public void deleteByID(Long aLong) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            Query delete = em.createNativeQuery("DELETE FROM Post WHERE ID = ?");
            delete.setParameter(1, aLong);
            delete.executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
        em.close();
    }
}
