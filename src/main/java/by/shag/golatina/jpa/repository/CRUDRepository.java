package by.shag.golatina.jpa.repository;

import java.util.List;

public interface CRUDRepository<E, ID> {

    E save(E entity);

    E findByID(ID id);

    List<E> findAll();

    E update(E entity);

    void deleteByID(ID id);
}
